<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>ANACompute - Home</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style><%@include file="css/style.css"%></style>

<body>
<!--Top navigation bar-->
<div class="topnav">
  <a class="active" href="Home.jsp"><img src="Images/ANACompute.png" alt="Compnay Logo" width="500" height="150"></a>
<!--Navigation bar links-->
  <a href="#news">Products</a>
  <a href="#contact">News</a>
  <a href="#about">Team</a>
  <a href="Login.jsp">Login</a>
  <a href="RegisterPage.jsp">Register</a>
</div>

<form class=searchSubmit action="Search" method="get">
<input type="text" name="searchValue" value="Search Products...">
<input type="submit" value="search">
</form><br>

<div style="padding-left:16px">
  <h2>Welcome to ANACompute! Your one stop shop for all computer hobbyist needs!</h2>
  <jsp:include page="Shop.xhtml"/>
 
 
</div>
</body>

</html>
