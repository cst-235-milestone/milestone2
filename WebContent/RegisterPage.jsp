<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style><%@include file="/css/style.css"%></style>
<title>ANACompute - Register Page</title>

</head>
<body>
<!--Top navigation bar-->
<div class="topnav">
  <a class="active" href="Home.jsp"><img src="Images/ANACompute.png" alt="Compnay Logo" width="500" height="150"></a>
<!--Navigation bar links-->
  <a href="#news">Products</a>
  <a href="#contact">News</a>
  <a href="#about">Team</a>
  <a href="Login.jsp">Login</a>
  <a href="RegisterPage.jsp">Register</a>
</div>
<form action="RegisterHandler" method="get">
<div>
	
	
	First Name: <br> 
	<input type="text" name="firstName" value=""> <%= request.getAttribute("Efirstname")%> 
	<br>
	Last Name:  <br>
	<input type="text" name="lastName" value=""> <%= request.getAttribute("Elastname")%>
	<br>
	Email: <br>
	<input type="text" name="Email" value=""> <%= request.getAttribute("EEmail")%> 
	<br>
	Address:<br>
	<input type="text" name="address" value=""> <%= request.getAttribute("Eaddress")%> 
	<br>
	PhoneNumber:<br>
	<input type="text" name="phoneNumber" value=""> <%= request.getAttribute("EphoneNumber")%> 
	<br>
	Username: <br>
	<input type="text" name="username" value=""> <%= request.getAttribute("Eusername")%>
	<br>
	Password:<br>
	<input type="text" name="password" value=""> <%= request.getAttribute("Epassword")%> 
	<br><br>
	<input type="submit" value='Submit'>
</div>

</form>
<form action="Login.jsp">
	<input type="submit" value="Back"/>
</form>
	
</body>

</html>