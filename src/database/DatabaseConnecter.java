package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.User;

public class DatabaseConnecter {

	public static ArrayList<User> users = new ArrayList<User>();
	public static int attempts = 3;
	
	
	

	public void InsertUser(User user) throws SQLException {

		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		PreparedStatement stmt = null;
		int rowsAffected = 0;
		c = DriverManager.getConnection(dbURL, username, password);

		// create a SQL statement
		stmt = c.prepareStatement(
				"insert into anacompute.user (firstName, lastName, email, address, phoneNumber, username, password) values (?,?,?,?,?,?,?)");
		stmt.setString(1, user.getFirstName());
		stmt.setString(2, user.getLastName());
		stmt.setString(3, user.getEmail());
		stmt.setString(4, user.getAddress());
		stmt.setString(5, user.getPhoneNumber());
		stmt.setString(6, user.getUsername());
		stmt.setString(7, user.getPassword());
		// execute the statemant
		rowsAffected = stmt.executeUpdate();

		// process the rows effected
		System.out.println("SUCCESS!! Rows affected " + rowsAffected);
		c.close();
		stmt.close();

	}

	public boolean check(String usName, String usPassword)  throws SQLException {
		boolean ct = false;
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			// Connect to database
			c = DriverManager.getConnection(dbURL, username, password);
			// create a SQL statement
			stmt = c.createStatement();
			// execute the statemant
			rs = stmt.executeQuery("select * from anacompute.user");
			// process the rows in the result set
			while (rs.next()) {
				if (rs.getString("username").equals(usName)
						&& rs.getString("password").equals(usPassword)) {
					ct = true;
				}

			}

		} catch (SQLException e) {
			System.out.println("Connection unsuccessful!");
			e.printStackTrace();
		} finally {
			// close the connection
			rs.close();

			stmt.close();

			c.close();

		}

		return ct;
	}

	public void readAll() throws SQLException {
		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			// Connect to database
			c = DriverManager.getConnection(dbURL, username, password);

			// create a SQL statement
			stmt = c.createStatement();
			// execute the statemant
			rs = stmt.executeQuery("select * from anacompute.user");

			// process the rows in the result set
			while (rs.next()) {
				//System.out.println(rs.getString("username"));
			}

		} catch (SQLException e) {
			System.out.println("Connection unsuccessful!");
			e.printStackTrace();
		} finally {
			// close the connection
			rs.close();

			stmt.close();

			c.close();

		}

	}

	public void deleteUser(User user) throws SQLException {
		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		PreparedStatement stmt = null;
		int rowsAffected = 0;

		try {
			// Connect to database
			c = DriverManager.getConnection(dbURL, username, password);

			// create a SQL statement
			stmt = c.prepareStatement("delete from anacompute.user where id = ?");
			// execute the statemant
			stmt.setString(1, user.getUsername());

			rowsAffected = stmt.executeUpdate();

			// process the rows effected
			System.out.println("Rows affected " + rowsAffected);

		} catch (SQLException e) {
			System.out.println("Connection unsuccessful!");
			e.printStackTrace();
		} finally {
			// close the connection
			stmt.close();

			c.close();

		}

	}

}
