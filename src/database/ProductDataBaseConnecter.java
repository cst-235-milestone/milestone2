package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import beans.Product;

import java.sql.Statement;

@ManagedBean
public class ProductDataBaseConnecter {

	public ArrayList<Product> getAllProducts() throws SQLException {
		System.out.println("GEt");
		return this.readAllProducts();
	}
	
	
	public void decreaseOne(Product pd) throws SQLException {
		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		int rowsAffected = 0;
		
		try {
			//Connect to database
			c = DriverManager.getConnection(dbURL, username, password);
			
			// create a SQL statement 
			stmt = c.createStatement();
			// execute the statemant
			String prep = "update anacompute.product set productAmount = '" + (pd.getProductAmount() - 1)+"' where productID = '" + pd.getProductID() +"'" ;
			System.out.println(pd.toString());
			rowsAffected = stmt.executeUpdate(prep);

			System.out.println(pd.toString());
		
			
			
			
			
		} catch (SQLException e) {
	
			e.printStackTrace();
		} finally {
			//close the connection
			stmt.close();
			
			c.close();
			
		}
		
		
		
	}
	

	public boolean checkProduct(Product pd) throws SQLException {
		boolean ct = false;
		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			// Connect to database
			c = DriverManager.getConnection(dbURL, username, password);
			//System.out.println("Connection successful!");
			//System.out.println("Connected to : " + dbURL);

			// create a SQL statement
			stmt = c.createStatement();
			// execute the statemant
			rs = stmt.executeQuery("select * from anacompute.product");

			// process the rows in the result set
			while (rs.next()) {
				if (rs.getInt("productID") == pd.getProductID()) {
					if (rs.getInt("productAmount") > 0) {
						//In Stock!
						System.out.println("This item is in stock!");
						decreaseOne(pd);
					} else {
						//Out of stock :(
						System.out.println("This item is out of stock!");
					}
				}

			}
		} catch (SQLException e) {
			System.out.println("Connection unsuccessful!");
			e.printStackTrace();
		} finally {
			// close the connection
			rs.close();
			stmt.close();
			c.close();
		}
		return ct;
		
		
	}

	public ArrayList<Product> readAllProducts() throws SQLException {
		// Connect to database
		String dbURL = "jdbc:mysql://localhost:3306/anacompute";
		String username = "root";
		String password = "root";

		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Product> p = new ArrayList<Product>();
		try {
			// Connect to database
			c = DriverManager.getConnection(dbURL, username, password);
			//System.out.println("Connection successful!");
		//	System.out.println("Connected to : " + dbURL);

			// create a SQL statement
			stmt = c.createStatement();
			// execute the statemant
			rs = stmt.executeQuery("select * from anacompute.product");

			// process the rows in the result set
			p.clear();
			while (rs.next()) {
				Product np = new Product();
				np.setProductID(Integer.parseInt(rs.getString("productID").toString()));
				np.setProductName(rs.getString("productName"));
				np.setProductDiscription(rs.getString("productDiscription"));
				np.setProductAmount(Integer.parseInt(rs.getString("productAmount").toString()));
				np.setProductCost(Double.parseDouble(rs.getString("productCost").toString()));
				np.setImageUrl(rs.getString("imageUrl"));
				//System.out.println(np.toString());
				p.add(np);

			}
		} catch (SQLException e) {
			//System.out.println("Connection unsuccessful!");
			e.printStackTrace();
		} finally {
			// close the connection
			rs.close();

			stmt.close();

			c.close();

		}
		return p;
	}

}
