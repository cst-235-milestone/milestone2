package database;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import beans.Product;

@ManagedBean
public class StoreManager {
	
	ArrayList<Product> shoppingCart = new ArrayList<Product>();
	
	public String purchase(Product c) throws SQLException {
		
		System.out.println("You clicked the submit button. Sending information to the database!" + c.toString());
		
		ProductDataBaseConnecter PDBC = new ProductDataBaseConnecter();
		if (PDBC.checkProduct(c)) {
			shoppingCart.add(c);
		}
		
		return "Home.jsp";
		
	}
	
	public String edit(Product c) {
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", c);
		return "Login.jsp";
	}
	
	

}
