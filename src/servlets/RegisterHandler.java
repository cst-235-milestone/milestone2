package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import database.DatabaseConnecter;
import database.DatabaseHash;

/**
 * Servlet implementation class RegisterHandler
 */
@WebServlet("/RegisterHandler")
public class RegisterHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String resultPage = "Login.jsp";
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String Email = request.getParameter("Email");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phoneNumber");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		User nu = new User();
		nu.setFirstName(firstName);
		nu.setLastName(lastName);
		nu.setEmail(Email);
		nu.setAddress(address);
		nu.setPhoneNumber(phoneNumber);
		nu.setUsername(username);
		nu.setPassword(password);
		boolean check = false;
		//firstname
		if (nu.getFirstName().equals("") || nu.getFirstName().equals(" ") ) {
			request.setAttribute("Efirstname"," Please enter your first name ");
			check = true;
		} else {
			request.setAttribute("Efirstname"," ");
			request.setAttribute("firstname",nu.getFirstName());
		}
		//lastname
		if (nu.getLastName().equals("") || nu.getLastName().equals(" ")) {
			request.setAttribute("Elastname"," Please enter your last name.");
			check = true;
		} else {
			request.setAttribute("Elastname"," ");
			request.setAttribute("lastname",nu.getLastName());
		}
		//Email
		if (nu.getEmail().equals("") || !nu.getEmail().contains("@") || !nu.getEmail().contains(".")) {
			request.setAttribute("EEmail", "Please enter a valid Email.");
			check = true;
		} else {
			request.setAttribute("EEmail"," ");
			request.setAttribute("Email",nu.getEmail());
		}
		//address
		if (nu.getAddress().equals("") || nu.getAddress().equals(" ")) {
			request.setAttribute("Eaddress", "Please enter a valid address.");
			check = true;
		} else {
			request.setAttribute("Eaddress"," ");
			request.setAttribute("address",nu.getAddress());
		}
		//phoneNumber
		if (nu.getPhoneNumber().equals("") || nu.getPhoneNumber().equals(" ") || nu.getPhoneNumber().length() < 10) {
			request.setAttribute("EphoneNumber", "Please enter a valid phone number.");
			check = true;
		} else {
			request.setAttribute("EphoneNumber"," ");
			request.setAttribute("phoneNumber",nu.getPhoneNumber());
		}
		//username
		if (nu.getUsername().equals("") || nu.getUsername().equals(" ")) {
			request.setAttribute("Eusername", "Please enter a username.");
			check = true;
		} else {
			request.setAttribute("Eusername", " ");
			request.setAttribute(username, nu.getUsername());
		}
		//password
		if (nu.getPassword().equals("") || nu.getPassword().equals(" ")) {
			request.setAttribute("Epassword", "Please enter a password.");
			check = true;
		} else {
			request.setAttribute("Epassword", " ");
			
		}
		DatabaseConnecter DBC = new DatabaseConnecter();
		
		try {
			if (DBC.check(nu.getUsername(), nu.getPassword())) {
				request.setAttribute("Eusername", "Username already in use, please enter a new username.");
				check = true;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		if (!check) {
			DatabaseHash.hdb.put(nu, DatabaseHash.hdb.size() + 1);
			//Insert to the online database;
			try {
				//Inserting to the page
				System.out.println("Insertinh in to the database.");
				DBC.InsertUser(nu);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} else {
			resultPage = "RegisterPage.jsp";
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher(resultPage).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		request.setAttribute("Efirstname", "*");
		request.setAttribute("Elastname", "*");
		request.setAttribute("EEmail", "*");
		request.setAttribute("Eaddress", "*");
		request.setAttribute("EphoneNumber", "*");
		request.setAttribute("Eusername", "*");
		request.setAttribute("Epassword", "*");
		request.setAttribute("firstname", "");
		request.setAttribute("lastname", "");
		request.setAttribute("Email", "");
		request.setAttribute("address", "");
		request.setAttribute("phoneNumber", "");
		request.setAttribute("username", "");
		request.setAttribute("password", "");
		request.getRequestDispatcher("RegisterPage.jsp").forward(request, response);
	}

}
